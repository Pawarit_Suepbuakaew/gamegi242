﻿using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Manager
{
    public class UIManager : MonoBehaviour
    {
        [SerializeField] private Button startButton;
        [SerializeField] private RectTransform startDialog;
        [SerializeField] private TextMeshProUGUI finalScoreText;
        [SerializeField] private Button restartButton;
        [SerializeField] private RectTransform endDialog;
        [SerializeField] private Button exit;
        
        
        private void Awake()
        {
            Debug.Assert(startButton != null, "startButton cannot be null");
            Debug.Assert(startDialog != null, "startDialog cannot be null");
            Debug.Assert(finalScoreText != null, "finalScoreText cannot null");
            Debug.Assert(restartButton != null, "restartButton cannot be null");
            Debug.Assert(endDialog != null, "endDialog cannot be null");   
            Debug.Assert(exit != null,"exit game");
            
            startButton.onClick.AddListener(OnStartButtonClicked);
            restartButton.onClick.AddListener(OnRestartButtonClicked);
            exit.onClick.AddListener(Exit);
        }

        private void Start()
        {
            GameManager.Instance.OnRestarted += RestartUI;
            ShowEndDialog(false); 
        }

        private void OnStartButtonClicked()
        {
            ShowStartDialog(false);
            GameManager.Instance.StartGame();
        }
    
        private void OnRestartButtonClicked()
        {
            ShowEndDialog(false);
            GameManager.Instance.StartGame();
        } 
    
       

        private void RestartUI()
        {
            ShowEndDialog(true);
        }
        

        private void ShowStartDialog(bool showDialog)
        {
            startDialog.gameObject.SetActive(showDialog);
        }

        private void ShowEndDialog(bool showDialog)
        {
            //UpdateScoreUI();
            endDialog.gameObject.SetActive(showDialog);
        }

        private void OnDestroy()
        {
            GameManager.Instance.OnRestarted -= RestartUI;
        }
        
        private void Exit()
        {
            Application.Quit();
        }
    }
}
